# Wide and deep project resources
The purpose of this repository is to store large files to be kept aside from the code of our main Wide and Deep repository.

So far it contains the trained weights for the Alexnet network for two specific datasets:

- Imagenet (imagenet.npy): This weights are the result of training the network on the Imagenet dataset, which include images for 1000 objects. They have been downloaded from [Michale Guerzhoy page](http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/).
- Places365 (places\_365.npy): This weights result in training Alexnet on the last version of the places dataset, which include images for 365 different types of scenes (e.g. bathroom, balcony). They have been downloaded from the [official Github page of the project](https://github.com/metalbubble/places365) and converted to Tensorflow using [caffe-tensorflow](https://github.com/ethereon/caffe-tensorflow).